package com.sampath.integration;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import org.springframework.http.ResponseEntity;

import org.springframework.web.client.RestTemplate;

import com.sampath.controller.model.AddUserResponse;
import com.sampath.controller.model.UserList;
import com.sampath.repository.UserRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerIntegrationTest {
	private static final Logger LOG = LoggerFactory.getLogger(UserControllerIntegrationTest.class);


	@LocalServerPort
	private int localPort;
	ResponseEntity<AddUserResponse> response = null;
	ResponseEntity<UserList[]> responseList = null;

	//using test tempalte these are not working
//	private String url = "http://USER-SERVICE/v1/users/1";
//	private String url2 = "http://USER-SERVICE/v1/users";
	
	//below is working but random port has some issue
	private String url = "http://localhost:3604/v1/users/1";
	private String url2 = "http://localhost:3604/v1/users";

	@Autowired
	private TestRestTemplate  restTemplate;

	HttpHeaders headers = new HttpHeaders();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	HttpEntity httpEntity = new HttpEntity(headers);

	@Test
	void test() {
		response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, AddUserResponse.class);
		LOG.info("response is: " + response);
	}

	@Test
	void testList() {

		responseList = restTemplate.exchange(url2, HttpMethod.GET, httpEntity, UserList[].class); // working

		List<UserList> usersList = Arrays.asList(responseList.getBody());
		// need to check this for the list
		LOG.info("responseList is: " + usersList);

	}
	
	

}
