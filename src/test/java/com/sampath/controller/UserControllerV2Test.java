package com.sampath.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(UserControllerV2.class)
public class UserControllerV2Test {
	private static final Logger LOG = LoggerFactory.getLogger(UserControllerV2Test.class);

	@Autowired
	MockMvc mockMvc;

	@MockBean
	SortAlgorithm sortAlgorithm;

	@Test
	void testGetUserById() throws Exception {
		when(sortAlgorithm.sort()).thenReturn("This is a Bubble sort method");
		RequestBuilder request = MockMvcRequestBuilders.get("/v2/users").accept(MediaType.APPLICATION_JSON);
		LOG.info("varala is: {}", mockMvc.perform(request).andReturn().getResponse().getContentAsString());
		mockMvc.perform(request).andExpect(status().isOk());
		assertEquals("This is a Bubble sort method",
				mockMvc.perform(request).andReturn().getResponse().getContentAsString());

	}

}
