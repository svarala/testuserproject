package com.sampath.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sampath.controller.model.AddUserRequest;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.exception.FailedToAddUserException;
import com.sampath.exception.FailedToGetUserException;
import com.sampath.exception.ResourceDeleteException;
import com.sampath.exception.UserNotFoundException;
import com.sampath.repository.UserRepository;
import com.sampath.service.UserService;
import com.sampath.service.model.AddUserServiceRequest;
import com.sampath.service.model.AddUserServiceResponse;
import com.sampath.service.model.UpdateUserServiceResponse;

//@SpringBootTest
@WebMvcTest(UserController.class)
////@RunWith(SpringRunner.class)
//@ExtendWith(MockitoExtension.class)
//@RunWith(MockitoJUnitRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
class UserControllerTest {

	private static final Logger LOG = LoggerFactory.getLogger(UserControllerTest.class);

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;
//	@Mock
//	private UserRepository userRepository;

	@Test
	void testGetUserById2() throws Exception {
		when(userService.getUserById(anyLong())).thenReturn(new AddUserServiceResponse(1L, "Sampath", 36));
//		doNothing().when(userRepository.findById(anyLong()));
		RequestBuilder request = MockMvcRequestBuilders.get("/v1/users/{userId}", 1L);
		mockMvc.perform(request).andExpect(status().isOk())
				.andExpect(content().json("{id:1,username:Sampath,age:36}", false));
		LOG.info("response string is: " + mockMvc.perform(request).andReturn().getResponse().getContentAsString());

	}

	@Test
	void saveUserTest() throws Exception {
		when(userService.addUser(any(AddUserServiceRequest.class)))
				.thenReturn(new AddUserServiceResponse(100L, "Nainu", 6));

//		new AddUserServiceRequest("Rama", 29)
//		any(AddUserServiceRequest.class)
		AddUserRequest addUser = new AddUserRequest("title6", 29);
		String addUserJson = new ObjectMapper().writeValueAsString(addUser);

		mockMvc.perform(
				MockMvcRequestBuilders.post("/v1/users").contentType(MediaType.APPLICATION_JSON).content(addUserJson))
				.andExpect(status().isCreated()).andExpect(content().json("{id:100,username:Nainu,age:6}"));

	}

	@Test
	void updateUserTest() throws Exception {
		when(userService.getUserById(anyLong())).thenReturn(new AddUserServiceResponse(1L, "Rama", 100));
		when(userService.updateUser(any(AddUserServiceRequest.class)))
				.thenReturn(new UpdateUserServiceResponse(100L, "Nainu", 6));
		AddUserRequest updateUser = new AddUserRequest("Nainu6", 7, 22L);
		String addUserJson = new ObjectMapper().writeValueAsString(updateUser);
		LOG.info("addUserJson value is: " + addUserJson);

		mockMvc.perform(
				MockMvcRequestBuilders.put("/v1/users").contentType(MediaType.APPLICATION_JSON).content(addUserJson))
				.andExpect(status().isCreated()).andExpect(content().json("{id:100,username:Nainu,age:6}"));

	}

	@Test
	void deleteByUserIdTest() throws Exception {
		Map<String, Boolean> responseMap = new HashMap<String, Boolean>();
		responseMap.put("deleted", Boolean.TRUE);
		when(userService.getUserById(anyLong())).thenReturn(new AddUserServiceResponse(22L, "Rama", 100));
		when(userService.deleteUser(anyLong())).thenReturn(responseMap);

		mockMvc.perform(MockMvcRequestBuilders.delete("/v1/users/{userId}", 22L)).andExpect(status().isOk())
				.andExpect(content().json("{deleted:true}"));
	}

}
