package com.sampath.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sampath.entity.WellnessUser;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.repository.UserRepository;
import com.sampath.service.model.AddUserServiceRequest;

//@SpringBootTest
//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(SpringExtension.class)
class DownStreamServiceTest {
	private static final Logger LOG = LoggerFactory.getLogger(DownStreamServiceTest.class);
	@Mock
     private UserRepository userRepository;
	
	@InjectMocks
	private DownStreamService downStreamService;
	
	@Test
	void testDownStreamServiceAddUserServiceRequest() throws DownStreamServiceException {	
//		Long userId = 1L;
		WellnessUser wellnessUser = new WellnessUser(1L,"Sampath",36);
		AddUserServiceRequest reqMock = new AddUserServiceRequest("Sampath",36);
//		Book newBook = new Book(1L, "Mockito Guide", "mkyong");
//        when(mockRepository.save(any(Book.class))).thenReturn(newBook);
		when(userRepository.save(any(WellnessUser.class))).thenReturn(wellnessUser);  //this is for save method
		//should override hashcode() and equals method
		LOG.info("reqMock is: "+reqMock.getAge());
		LOG.info("reqMock is: "+reqMock.getUsername());
		WellnessUser user= downStreamService.downStreamService(reqMock);
		assertNotNull(user);		
		assertEquals(1L,user.getUserId());
		assertEquals(reqMock.getUsername(),user.getUsername());
		assertEquals(reqMock.getAge(),user.getAge());
	}

}
