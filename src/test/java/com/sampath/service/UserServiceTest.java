package com.sampath.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sampath.entity.WellnessUser;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.exception.FailedToAddUserException;
import com.sampath.exception.FailedToGetUserException;
import com.sampath.exception.ResourceDeleteException;
import com.sampath.exception.UserNotFoundException;
import com.sampath.repository.UserRepository;
import com.sampath.service.model.AddUserServiceRequest;
import com.sampath.service.model.AddUserServiceResponse;
import com.sampath.service.model.UpdateUserServiceResponse;

//@SpringBootTest
//@RunWith(MockitoJUnitRunner.class)  --junit4
//@ContextConfiguration(classes = {UserRepository.class})
//https://stackoverflow.com/questions/55276555/when-to-use-runwith-and-when-extendwith
@ExtendWith(SpringExtension.class)
class UserServiceTest {
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private IUserService userService = new UserService(); // for interface
//	private UserService userService; //for Class

	@Mock
	private DownStreamService downStreamService;

	@Test
	void testGetUserById() throws UserNotFoundException, FailedToGetUserException {
		Long userId = 1L;
		WellnessUser e1ForMock = new WellnessUser(userId, "Rama", 29);
//	    doReturn(Optional.of(e1ForMock)).when(userRepository).findById(userId);
		when(userRepository.findById(anyLong())).thenReturn(Optional.of(e1ForMock));
//	    when(userRepository.findById(anyNumber())).thenReturn(Optional.of(e1ForMock));  
		AddUserServiceResponse addUserServiceResponse = userService.getUserById(userId);
		assertNotNull(addUserServiceResponse, "Employee with employeeId : " + userId + " not found");
		assertEquals(userId, addUserServiceResponse.getUserId());
		assertEquals(e1ForMock.getUsername(), addUserServiceResponse.getUsername());
		assertEquals(e1ForMock.getAge(), addUserServiceResponse.getAge());

	}

	@Test
	void addUserTest() throws DownStreamServiceException, FailedToAddUserException {
		Long userId = 1L;
		AddUserServiceRequest addUserServiceReqMock = new AddUserServiceRequest("Sampath", 36);
		when(downStreamService.downStreamService(addUserServiceReqMock))
				.thenReturn(new WellnessUser(userId, "Sampath", 36));
		AddUserServiceResponse addUserServiceResponse = userService.addUser(addUserServiceReqMock);
		assertNotNull(addUserServiceResponse);
		assertEquals(userId, addUserServiceResponse.getUserId());
		assertEquals(addUserServiceReqMock.getUsername(), addUserServiceResponse.getUsername());
		assertEquals(addUserServiceReqMock.getAge(), addUserServiceResponse.getAge());

	}

	@Test
	void deleteUserTest() throws ResourceDeleteException {
		Long userId = 22L;
//		way-1(perfect one)
		userService.deleteUser(userId);
		verify(userRepository, times(1)).deleteById(eq(userId));

//		doNothing().doThrow(new RuntimeException("this is for testMethod")).when(userService).testDoNothing(); --error as userService is not a mock
		
//		verify(userService,times(1)).testDoNothing();  error as userService is not a mock
		

//		Way-2(this is also working)
//		doNothing().when(userRepository).deleteById(anyLong());
//		Map<String, Boolean> responseMap = userService.deleteUser(userId);
//		assertEquals(true, responseMap.get("deleted"));

	}

	@Test
	@DirtiesContext
	void deleteUserTest1() throws ResourceDeleteException {
		Long userId = 22L;
		userRepository.deleteById(userId);
	}

	@Test
	void updateUserTest() {
		Long userId = 1L;
		AddUserServiceRequest addUserServiceReqMock = new AddUserServiceRequest("Sampath", 36);
		when(userRepository.save(any(WellnessUser.class))).thenReturn(new WellnessUser(userId, "Sampath", 36));
		UpdateUserServiceResponse updateUserServiceResponse = userService.updateUser(addUserServiceReqMock);
		assertNotNull(updateUserServiceResponse);
		assertEquals(userId, updateUserServiceResponse.getUserId());
		assertEquals(addUserServiceReqMock.getUsername(), updateUserServiceResponse.getUsername());
		assertEquals(addUserServiceReqMock.getAge(), updateUserServiceResponse.getAge());

	}

}
