package com.sampath.entity;

import java.util.Objects;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;

@Entity
public class WellnessUser {
	@Id
	@GeneratedValue
	private Long userId;

	@Column(name = "user_name")
	@Size(min=2, max=30)
	private String username;
	@NotNull
	private int age;


	public WellnessUser() {
		super();
	}

//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "address_id", referencedColumnName = "AddressId")
//	private Address address;

	public WellnessUser(String username, int age) {		
		this.username = username;
		this.age = age;
	}

	public WellnessUser(Long userId2, String userName, int i) {		
		this.userId = userId2;
		this.username = userName;
		this.age = i;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, userId, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WellnessUser other = (WellnessUser) obj;
		return age == other.age && Objects.equals(userId, other.userId) && Objects.equals(username, other.username);
	}

//	public Address getAddress() {
//		return address;
//	}
//
//	public void setAddress(Address address) {
//		this.address = address;
//	}

}
