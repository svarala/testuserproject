package com.sampath.entity;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Review {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "review_generator")
	@SequenceGenerator(name = "review_generator", sequenceName = "review_seq", allocationSize=1)
	private Long reviewId;
//	@Column(name="review_desc",updatable=false,nullable=false)
	private String reviewDesc;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id",referencedColumnName ="productId")
	@JsonIgnoreProperties("reviews")	
	private Product product;

	protected Review() {
	}

	public Review(Long reviewId, String reviewDesc) {
		super();
		this.reviewId = reviewId;
		this.reviewDesc = reviewDesc;
	}

	public Long getReviewId() {
		return reviewId;
	}

	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}

	public String getReviewDesc() {
		return reviewDesc;
	}

	public void setReviewDesc(String reviewDesc) {
		this.reviewDesc = reviewDesc;
	}

	@Override
	public int hashCode() {
		return Objects.hash(reviewDesc, reviewId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Review other = (Review) obj;
		return Objects.equals(reviewDesc, other.reviewDesc) && Objects.equals(reviewId, other.reviewId);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "Review [reviewId=" + reviewId + ", reviewDesc=" + reviewDesc + ", product=" + product + "]";
	}

	

}
