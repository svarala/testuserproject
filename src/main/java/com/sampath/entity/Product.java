package com.sampath.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
//@Table(name = "product")

@NamedQuery(name = "only_product_data", query = "select c from Product c where c.productId = :productId")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long productId;
	String productName;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy="product")	//default is LAZY
	@JsonIgnoreProperties("product")
	List<Review> reviews = new ArrayList<>();

	public Product() {
	}

	public Product(String productName, List<Review> reviews) {
		super();
		this.productName = productName;
		this.reviews = reviews;
	}

	public Product(Long productId, String productName, List<Review> reviews) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.reviews = reviews;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void addReview(Review review) {
		this.reviews.add(review);
	}

	public void removeReview(Review review) {
		this.reviews.remove(review);
	}

}
