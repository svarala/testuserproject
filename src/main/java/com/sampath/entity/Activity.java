package com.sampath.entity;

public class Activity {

	private Long id;

	private String name;
	private Double activeTime;

	private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(Double activeTime) {
		this.activeTime = activeTime;
	}

	@Override
	public String toString() {
		return "Activity [id=" + id + ", name=" + name + ", activeTime=" + activeTime + "]";
	}

	public Activity(Long id, String name, Double activeTime) {
		super();
		this.id = id;
		this.name = name;
		this.activeTime = activeTime;
	}

	public Activity() {

	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
