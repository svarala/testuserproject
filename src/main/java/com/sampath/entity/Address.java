package com.sampath.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long addressId;
	private String line1;
	private String line2;
	private Long pincode;
	@OneToOne(fetch= FetchType.LAZY)
//	@OneToOne(cascade = CascadeType.ALL) //if child and parent relationship dont user the cascade type all	
    @JoinColumn(name = "user_id", referencedColumnName = "userId")
	private WellnessUser user;

	public Address() {
		super();
	}

	public Address(String line1, String line2, Long pincode,WellnessUser user) {
		super();
//		this.addressId = addressId;
		this.user = user;		
		this.line1 = line1;
		this.line2 = line2;
		this.pincode = pincode;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public Long getAddressId() {
		return addressId;
	}

	public WellnessUser getUser() {
		return user;
	}

	public void setUser(WellnessUser user) {
		this.user = user;
	}



}
