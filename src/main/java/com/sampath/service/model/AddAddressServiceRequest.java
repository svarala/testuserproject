package com.sampath.service.model;

import com.sampath.entity.WellnessUser;

public class AddAddressServiceRequest {

	private String line1;
	private String line2;
	private Long pinCode;
	private WellnessUser user;

	public AddAddressServiceRequest(String line1, String line2, Long pinCode, WellnessUser user) {
		super();
		this.line1 = line1;
		this.line2 = line2;
		this.pinCode = pinCode;
		this.user = user;
	}

	public AddAddressServiceRequest() {
		super();
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public Long getPinCode() {
		return pinCode;
	}

	public void setPinCode(Long pinCode) {
		this.pinCode = pinCode;
	}

	public WellnessUser getUser() {
		return user;
	}

	public void setUser(WellnessUser user) {
		this.user = user;
	}

	

}
