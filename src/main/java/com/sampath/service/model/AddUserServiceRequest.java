package com.sampath.service.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class AddUserServiceRequest {
//	@XmlElement
	private String username;
//	@XmlElement
	private int age;
	private Long id;

	public AddUserServiceRequest() {
		super();
	}

	public AddUserServiceRequest(String username, int age) {
		// TODO Auto-generated constructor stub

		this.username = username;
		this.age = age;
	}

	public AddUserServiceRequest(Long id, String username, int age) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.username = username;
		this.age = age;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
