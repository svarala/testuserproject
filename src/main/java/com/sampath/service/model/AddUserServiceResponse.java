package com.sampath.service.model;

public class AddUserServiceResponse {

	private String username;
	private int age;
	private Long userId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public AddUserServiceResponse(Long userId, String username, int age) {
		super();
		this.userId = userId;
		this.username = username;
		this.age = age;
	}

	public AddUserServiceResponse() {
		// TODO Auto-generated constructor stub
	}

	public void setAge(int age) {
		this.age = age;
	}


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
