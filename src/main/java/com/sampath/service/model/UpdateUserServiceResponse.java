package com.sampath.service.model;

import java.util.Objects;

public class UpdateUserServiceResponse {
	private String username;
	private int age;
	private Long userId;

	public UpdateUserServiceResponse() {
		super();
	}

	public UpdateUserServiceResponse(Long id, String username, int age) {
		super();
		this.username = username;
		this.age = age;
		this.userId = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, userId, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateUserServiceResponse other = (UpdateUserServiceResponse) obj;
		return age == other.age && Objects.equals(userId, other.userId) && Objects.equals(username, other.username);
	}

}
