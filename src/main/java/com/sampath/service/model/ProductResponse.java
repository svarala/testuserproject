package com.sampath.service.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.sampath.entity.Review;

public class ProductResponse {

	Long productId;
	String productName;
	List<Review> reviews = new ArrayList<>();

	public ProductResponse() {
		super();
	}

	public ProductResponse(Long productId, String productName, List<Review> reviews) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.reviews = reviews;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void addReview(Review review) {
		this.reviews.add(review);
	}

	@Override
	public int hashCode() {
		return Objects.hash(productId, productName, reviews);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductResponse other = (ProductResponse) obj;
		return Objects.equals(productId, other.productId) && Objects.equals(productName, other.productName)
				&& Objects.equals(reviews, other.reviews);
	}

}
