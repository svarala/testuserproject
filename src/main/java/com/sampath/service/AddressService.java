package com.sampath.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sampath.entity.Address;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.repository.AddressRepository;
import com.sampath.service.model.AddAddressServiceRequest;
import com.sampath.service.model.AddAddressServiceResponse;

import oracle.jdbc.logging.annotations.Log;


@Service
public class AddressService {

	private static final Logger LOG = LoggerFactory.getLogger(AddressService.class);
	@Autowired
	private AddressRepository addressRepository;
	AddAddressServiceResponse addAddressServiceResponse= new AddAddressServiceResponse();

	public AddAddressServiceResponse addAddress(AddAddressServiceRequest serviceReq) throws DownStreamServiceException {

		Address AddressReq = new Address(serviceReq.getLine1(), serviceReq.getLine2(), serviceReq.getPinCode(),
				serviceReq.getUser());

		try {
			LOG.info("before saveing the address");
			Address address = addressRepository.save(AddressReq);
			LOG.info("After saveing the address");
			addAddressServiceResponse.setAddressId(address.getAddressId());
			addAddressServiceResponse.setLine1(address.getLine1());
			addAddressServiceResponse.setLine2(address.getLine2());
			addAddressServiceResponse.setPinCode(address.getPincode());
			addAddressServiceResponse.setUser(address.getUser());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DownStreamServiceException("Exception while saving address records" + e.getMessage());
		}
		return addAddressServiceResponse;
	}

}
