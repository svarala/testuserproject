package com.sampath.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.sampath.entity.WellnessUser;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.exception.UserNotFoundException;
import com.sampath.repository.UserRepository;
import com.sampath.service.model.AddUserServiceRequest;

@Service
public class DownStreamService {
	private static final Logger LOG = LoggerFactory.getLogger(DownStreamService.class);
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	private UserRepository userRepository;

	@Transactional
	public WellnessUser downStreamService(AddUserServiceRequest req) throws DownStreamServiceException {
		WellnessUser user = new WellnessUser();
//		WellnessUser user = null;
		try {

			userRepository.save(new WellnessUser(req.getUsername(), req.getAge()));
			LOG.info("user value is: " + user);

		} catch (Exception e) {
			e.printStackTrace();
			throw new DownStreamServiceException("Exception from downstream service" + e.getMessage());
		}
		return user;
	}

	public WellnessUser downStreamService(Long userId) throws UserNotFoundException {

		WellnessUser user = userRepository.findById(userId)
				.orElseThrow(() -> new UserNotFoundException("User id: " + userId + " is not found"));
		return user;
	}
}
