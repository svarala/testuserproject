package com.sampath.service;

import java.util.List;
import java.util.Map;

import com.sampath.controller.model.AddUserRequest;
import com.sampath.controller.model.AddUserResponse;
import com.sampath.controller.model.UserList;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.exception.FailedToAddUserException;
import com.sampath.exception.FailedToGetUserException;
import com.sampath.exception.ResourceDeleteException;
//import com.sampath.exception.GlobalException;
import com.sampath.exception.UserNotFoundException;
import com.sampath.service.model.AddUserServiceRequest;
import com.sampath.service.model.AddUserServiceResponse;
import com.sampath.service.model.UpdateUserServiceResponse;


public interface IUserService {

	AddUserServiceResponse addUser(AddUserServiceRequest req)
			throws DownStreamServiceException, FailedToAddUserException;

	AddUserServiceResponse getUserById(Long userId) throws UserNotFoundException, FailedToGetUserException;

	List<UserList> getUserList();

	UpdateUserServiceResponse updateUser(AddUserServiceRequest serviceReq);

	Map<String, Boolean> deleteUser(Long userId) throws ResourceDeleteException;
	void testDoNothing();

	AddUserResponse updateUser1(AddUserRequest request) throws UserNotFoundException;

}
