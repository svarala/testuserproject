package com.sampath.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.sampath.entity.Product;
import com.sampath.exception.InternalServerErrorException;
import com.sampath.service.model.ProductResponse;

public interface IProductService {

	List<ProductResponse> getProductsFromDb();

	Product getProductById(Long Id) throws InternalServerErrorException;

	Product getProductByIdUsingNamedQuery(Long productId) throws InternalServerErrorException;
	Long getTotalCarsByModel(Long Id);

	Page<Product> testPagination();

	Page<Product> getProductByIdAndPage(Long productId);

	

}
