package com.sampath.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sampath.entity.Product;
import com.sampath.entity.Review;
import com.sampath.exception.InternalServerErrorException;
import com.sampath.repository.ITestProcedureRepository;
import com.sampath.repository.ProductRepository;
import com.sampath.service.model.ProductResponse;

@Service
public class productService implements IProductService {
	private static final Logger LOG = LoggerFactory.getLogger(productService.class);

	@Autowired
	ProductRepository productRepository;
	@Autowired
	ITestProcedureRepository testProcedureRepository;

	@Override
//	@Transactional
	public List<ProductResponse> getProductsFromDb() {
		List<ProductResponse> productResponseList = new ArrayList<>();
		List<Product> products = new ArrayList<>();
		productRepository.findAll().forEach(products::add);

		LOG.info("product[0] reviews are: {}", products.get(0).getReviews());

		List<Review> reviewList = products.get(0).getReviews();

		reviewList.stream().forEach(System.out::println);

//		List<ProductResponse> productResponseList = products.stream().map(p -> new ProductResponse(p.getProductId(),p.getProductName(),p.getReviews())).collect(Collectors.toList());
//		LOG.info("product list is: {}",products);
//		for (Product product : products) {
//			productResponseList
//					.add(new ProductResponse(product.getProductId(), product.getProductName(), product.getReviews()));
//			LOG.info(product.getProductName()+""+product.getProductId()+""+product.getReviews());
////			for (Review review : product.getReviews()) {
//////				LOG.info("review is: {}",review.get);
////			}
//		}
		return productResponseList;

	}
@Override
	public Product getProductById(Long Id) throws InternalServerErrorException {
		Product product = productRepository.findById(Id)
				.orElseThrow(() -> new InternalServerErrorException("product is not available"));
		return product;
	}

	@Override
	public Product getProductByIdUsingNamedQuery(Long productId) throws InternalServerErrorException {
		Product product = productRepository.findProductById(productId)
				.orElseThrow(() -> new InternalServerErrorException("product is not available using named query"));
		return product;
	}
	@Override
	public Long getTotalCarsByModel(Long Id) {
		// TODO Auto-generated method stub		
		return testProcedureRepository.getTotalCarsByModel(Id,0L);
	}
	
	@Override
	public Page<Product> testPagination() {
		// TODO Auto-generated method stub		
		Pageable sortedByName = 
				  PageRequest.of(0, 3, Sort.by("productName"));
		return testProcedureRepository.findAll(sortedByName);
	}
	@Override
	public Page<Product> getProductByIdAndPage(Long productId) {
		// TODO Auto-generated method stub
		Pageable sortedByName = 
				  PageRequest.of(0, 3, Sort.by("productId"));
		return productRepository.findProductById(productId, sortedByName);
	}
	
	

}
