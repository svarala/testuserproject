package com.sampath.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sampath.controller.UserController;
import com.sampath.controller.model.AddUserRequest;
import com.sampath.controller.model.AddUserResponse;
import com.sampath.controller.model.UserList;
import com.sampath.entity.WellnessUser;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.exception.FailedToAddUserException;
import com.sampath.exception.FailedToGetUserException;
import com.sampath.exception.ResourceDeleteException;
import com.sampath.exception.UserNotFoundException;
import com.sampath.repository.UserRepository;
import com.sampath.service.model.AddUserServiceRequest;
import com.sampath.service.model.AddUserServiceResponse;
import com.sampath.service.model.UpdateUserServiceResponse;

@Service("IUserService")
public class UserService implements IUserService {
	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	@Autowired
	DownStreamService downStreamService;
	@Autowired
	UserRepository userRepository;

	public AddUserServiceResponse addUser(AddUserServiceRequest addUserServiceRequest)
			throws DownStreamServiceException, FailedToAddUserException {
		AddUserServiceResponse response = new AddUserServiceResponse();
		try {
			WellnessUser user = downStreamService.downStreamService(addUserServiceRequest);
			response.setUsername(user.getUsername());
			response.setAge(user.getAge());
			response.setUserId(user.getUserId());
		} catch (DownStreamServiceException exception) {
			throw exception;
		} catch (Exception e) {
			e.printStackTrace();
			throw new FailedToAddUserException(e.getMessage());
		}
		return response;

	}

	public AddUserServiceResponse getUserById(Long userId) throws UserNotFoundException, FailedToGetUserException {
		AddUserServiceResponse addUserServiceResponse = new AddUserServiceResponse();
		try {
			WellnessUser user = userRepository.findById(userId)
					.orElseThrow(() -> new UserNotFoundException("user is not available"));
			addUserServiceResponse = new AddUserServiceResponse();
			addUserServiceResponse.setAge(user.getAge());
			addUserServiceResponse.setUserId(userId);
			addUserServiceResponse.setUsername(user.getUsername());
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("In FailedToGetUserException");
			throw new FailedToGetUserException(e.getMessage());
		}
		return addUserServiceResponse;
	}

	public List<UserList> getUserList() {
		List<WellnessUser> users = new ArrayList<>();
		List<UserList> userList = new ArrayList<>();
		LOG.info("Before userService list");
		userRepository.findAll().forEach(users::add);
		LOG.info("After userRepository.findAll() method, list size is: {}", users.size());
		for (WellnessUser user : users) {
			userList.add(new UserList(user.getUserId(), user.getUsername(), user.getAge()));
		}

		return userList;
	}

	@Override
	public UpdateUserServiceResponse updateUser(AddUserServiceRequest serviceReq) {
		WellnessUser user = userRepository
				.save(new WellnessUser(serviceReq.getId(), serviceReq.getUsername(), serviceReq.getAge()));
		return new UpdateUserServiceResponse(user.getUserId(), user.getUsername(), user.getAge());
	}

	@Override
	public Map<String, Boolean> deleteUser(Long userId) throws ResourceDeleteException {
		Map<String, Boolean> response = new HashMap<>();
		testDoNothing();
		try {
			userRepository.deleteById(userId);
			response.put("deleted", Boolean.TRUE);
		} catch (Exception e) {
			throw new ResourceDeleteException("Error while delting the resource");
		}
		return response;

	}

	@Override
	public void testDoNothing() {
		LOG.info("This method is to test the doNothing functionality in Mockito");
	}

	@Override
	@Transactional  //better to use for parent and child relationship
	//if transactional is not there, user.setAge() will not save into the database
	public AddUserResponse updateUser1(AddUserRequest request) throws UserNotFoundException {
		WellnessUser user = userRepository.findById(request.getUserId())
				.orElseThrow(() -> new UserNotFoundException("user is not available"));
		user.setAge(request.getAge());  //need to check with Sreekar/ Praveen
		AddUserResponse addUserResponse = new AddUserResponse();
		addUserResponse.setId(user.getUserId());
		addUserResponse.setUsername(user.getUsername());
		addUserResponse.setAge(user.getAge());
		return addUserResponse;
	}

}
