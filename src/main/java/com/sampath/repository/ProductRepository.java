package com.sampath.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sampath.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	@Query(name = "only_product_data")
	Optional<Product> findProductById(@Param(value = "productId") Long productId);
	
	@Query(name = "only_product_data")
	Page<Product> findProductById(@Param(value = "productId") Long productId,Pageable page);
	
	

}
