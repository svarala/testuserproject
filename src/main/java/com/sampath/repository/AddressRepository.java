package com.sampath.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Service;

import com.sampath.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
