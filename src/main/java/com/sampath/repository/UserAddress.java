package com.sampath.repository;

public interface UserAddress {

	Long getUserId();

	String getUserName();

	int getAge();

	String getLine1();

//	private Long userId;
//	private String userName;
//	private int age;
//	private String line1;
//	public UserAddress() {
//		super();
//	}
//	public UserAddress(Long userId, String userName, int age, String line1) {
//		super();
//		this.userId = userId;
//		this.userName = userName;
//		this.age = age;
//		this.line1 = line1;
//	}
//	public Long getUserId() {
//		return userId;
//	}
//	public void setUserId(Long userId) {
//		this.userId = userId;
//	}
//	public String getUserName() {
//		return userName;
//	}
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	public int getAge() {
//		return age;
//	}
//	public void setAge(int age) {
//		this.age = age;
//	}
//	public String getLine1() {
//		return line1;
//	}
//	public void setLine1(String line1) {
//		this.line1 = line1;
//	}
//	@Override
//	public String toString() {
//		return "UserAddress [userId=" + userId + ", userName=" + userName + ", age=" + age + ", line1=" + line1 + "]";
//	}

}
