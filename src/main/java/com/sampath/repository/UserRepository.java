package com.sampath.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Service;

import com.sampath.entity.WellnessUser;

@Repository
public interface UserRepository extends JpaRepository<WellnessUser, Long> {
	@Query(value = "select w.user_id userId,w.user_name userName,w.age,a.line1 from wellness_user w, address a where w.user_id = :user_id and a.user_id(+) = w.user_id", nativeQuery = true)
	public List<UserAddress> findUserWithAddress(@Param(value = "user_id") Long userId);
	
//	@Query(new UserAddress1("select w.user_id userId,w.user_name userName,w.age,a.line1 from wellness_user w, address a where w.user_id = :user_id and a.user_id(+) = w.user_id"))
//	public List<UserAddress> findUserWithAddress1(@Param(value = "user_id") Long userId);

}
