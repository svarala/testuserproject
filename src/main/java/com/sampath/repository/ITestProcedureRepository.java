package com.sampath.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.sampath.entity.Product;

public interface ITestProcedureRepository extends JpaRepository<Product,Long> {
	@Procedure("count_comments")
	int getTotalCarsByModel1(@Param("postId") Long postId);
	
	@Query(value= "{call count_comments(:postId,:commentCount)}",nativeQuery=true)
	Long getTotalCarsByModel(@Param("postId") Long postId,@Param("commentCount") Long commentCount);
	


}
