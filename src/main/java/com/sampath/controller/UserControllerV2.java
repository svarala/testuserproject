package com.sampath.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sampath.entity.Activity;
import com.sampath.exception.DownStreamServiceException;
import com.sampath.service.model.TestScopes;

@RestController
@RequestMapping("/v2/users")
public class UserControllerV2 {
	private static Logger LOG = LoggerFactory.getLogger(UserControllerV2.class);

	@Autowired
	@Qualifier("bubble")
//	SortAlgorithm bubbleSort;
	SortAlgorithm sortAlgorithm;

//	@Autowired
//	TestScopes testScope;

	@Autowired
	RestTemplate restTemplate;

	@GetMapping
	public String getSortMethod() {
//		String s = restTemplate.getForObject("https://howtodoinjava.com/junit5/junit-5-vs-junit-4/",String.class);
//		System.out.println(s);

		return sortAlgorithm.sort();
//		return bubbleSort.sort();

	}

	//if you give @RequestBody annotation, input should be required
//	public ResponseEntity<Activity> insertInDiffMicroservice(@RequestBody Activity testActivity) throws URISyntaxException, DownStreamServiceException {
	@PostMapping
	public ResponseEntity<Activity> insertInDiffMicroservice() throws URISyntaxException, DownStreamServiceException {
		ResponseEntity<Activity> response = null;
		URI url = null;
		try {
			url = new URI("http:/localhost:1004/activities");
		} catch (URISyntaxException u) {
			new Exception("wrong url : " + u.getMessage());
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		Activity activity = new Activity();
		activity.setActiveTime(10.0d);
		activity.setName("Test");
		activity.setUserId(1L);

		HttpEntity<Activity> requestEntity = new HttpEntity<>(activity, headers);
//		restTemplate.postForEntity( "http://localhost:1004/activities", requestEntity,Activity.class); --working fine
		try {
			LOG.info("ENTERED IN REST TEMPLATE TRY");
//			response = restTemplate.postForEntity(url, requestEntity, Activity.class); //exchange
			response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Activity.class);
		}

		catch (Exception e) {
			e.printStackTrace();
			LOG.error("ENTERED IN REST TEMPLATE catch ");
			throw new DownStreamServiceException("Error from the downstream" + e.getMessage());
		}

		return new ResponseEntity<Activity>(response.getBody(), HttpStatus.CREATED);
	}

}
