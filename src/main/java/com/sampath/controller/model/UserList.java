package com.sampath.controller.model;

import java.io.Serializable;

import com.sampath.entity.WellnessUser;

public class UserList extends WellnessUser{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userId;
	private String username;
	private int age;

	public UserList() {
		super();
	}

	public UserList(Long userId, String username, int age) {
		this.userId = userId;
		this.username = username;
		this.age = age;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "UserList [userId=" + userId + ", username=" + username + ", age=" + age + "]";
	}

}
