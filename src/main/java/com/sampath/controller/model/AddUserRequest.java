package com.sampath.controller.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "user")
@XmlAccessorType(XmlAccessType.NONE)
public class AddUserRequest {
//@XmlAttribute
//	private Long userId;
	@XmlElement
	private String username;
	@XmlElement
	private int age;
	
	private Long userId;

	public AddUserRequest() {
		super();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public AddUserRequest(String username, int age) {
		super();
		this.username = username;
		this.age = age;
	}

	public AddUserRequest(String username, int age, Long userId) {
		super();
		this.username = username;
		this.age = age;
		this.userId = userId;
	}

}
