package com.sampath.controller.model;

public class AddAddressRequest {
	private String line1;
	private String line2;
	private Long pinCode;
	public AddAddressRequest() {
		super();
	}

	private Long userId;

	public AddAddressRequest(String line1, String line2, Long pinCode, Long userId) {
		super();
		this.line1 = line1;
		this.line2 = line2;
		this.pinCode = pinCode;
		this.userId = userId;
	}

	public AddAddressRequest(String line1, String line2, Long pinCode) {
		super();
		this.line1 = line1;
		this.line2 = line2;
		this.pinCode = pinCode;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public Long getPinCode() {
		return pinCode;
	}

	public void setPinCode(Long pinCode) {
		this.pinCode = pinCode;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
