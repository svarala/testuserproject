package com.sampath.controller.model;

import com.sampath.entity.WellnessUser;

public class AddAddressResponse {
	private Long addressId;
	private String line1;
	private String line2;
	private Long pinCode;
	private WellnessUser user;

	public AddAddressResponse(Long addressId, String line1, String line2, Long pinCode, WellnessUser user) {
		super();
		this.addressId = addressId;
		this.line1 = line1;
		this.line2 = line2;
		this.pinCode = pinCode;
		this.user = user;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public Long getPinCode() {
		return pinCode;
	}

	public void setPinCode(Long pinCode) {
		this.pinCode = pinCode;
	}

	public AddAddressResponse() {
		super();
	}

	public void setUser(WellnessUser user) {

		this.user = user;

	}

	public WellnessUser getUser() {
		return user;
	}

}
