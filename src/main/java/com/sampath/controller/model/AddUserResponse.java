package com.sampath.controller.model;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement (name = "user")
@XmlAccessorType(XmlAccessType.NONE)
public class AddUserResponse {
	@XmlElement
	private String username;
	@XmlElement
	private int age;
//	@XmlAttribute
	@XmlElement
	private Long Id;

	public String getUsername() {
		return username;
	}

	public AddUserResponse() {
		super();
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	@Override
	public String toString() {
		return "AddUserResponse [username=" + username + ", age=" + age + ", Id=" + Id + "]";
	}

}
