package com.sampath.controller.model;

import java.util.ArrayList;
import java.util.List;

public class UsersList {
	private List<UserList>  usersList;

	public UsersList() {
		this.usersList = new ArrayList<>();
	}

	public List<UserList> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<UserList> usersList) {
		this.usersList = usersList;
	}

	@Override
	public String toString() {
		return "UsersList [usersList=" + usersList + "]";
	}

}
