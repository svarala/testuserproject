package com.sampath.controller;

import java.util.List;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sampath.entity.Product;
import com.sampath.exception.GlobalException;
import com.sampath.service.IProductService;
import com.sampath.service.model.ProductResponse;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	IProductService productService;

	@GetMapping
	public ResponseEntity<List<ProductResponse>> getProducts() {
		List<ProductResponse> productResponseList = productService.getProductsFromDb();
		return new ResponseEntity<List<ProductResponse>>(productResponseList, HttpStatus.OK);
	}

	@GetMapping("/{productId}")
	public ResponseEntity<Product> getProductById(@PathVariable Long productId) throws GlobalException {
		Product product = productService.getProductById(productId);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@GetMapping("/test/{productId}")
	public ResponseEntity<Product> getProductByIdUsingNamedQuery(@PathVariable Long productId) throws GlobalException {
		Product product = productService.getProductByIdUsingNamedQuery(productId);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@GetMapping("/testProc/{productId}")
	public Long testProcedure(@PathVariable Long productId) throws GlobalException {
//		int vId= 0;
		Long product = productService.getTotalCarsByModel(productId);
		return product;
//		return new ResponseEntity<int>(product, HttpStatus.OK);
	}

	@GetMapping("/page")
	public Page<Product> getAllByPage() throws GlobalException {
		return productService.testPagination();

	}

	@GetMapping("/page/{productId}")
	public ResponseEntity<Page<Product>> getProductByIdAndPage(@PathVariable Long productId) throws GlobalException {
		Page<Product> product = productService.getProductByIdAndPage(productId);
		return new ResponseEntity<Page<Product>>(product, HttpStatus.OK);
	}
	
	@GetMapping("/page1/{productId}")
	public ResponseEntity<Page<Product>> getProductByIdAndPage(@QueryParam(value = "page") int pageWith,@PathVariable Long productId) throws GlobalException {
		Page<Product> product = productService.getProductByIdAndPage(productId);
		return new ResponseEntity<Page<Product>>(product, HttpStatus.OK);
	}

}
