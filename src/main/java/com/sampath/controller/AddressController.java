package com.sampath.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sampath.controller.model.AddAddressRequest;
import com.sampath.controller.model.AddAddressResponse;
import com.sampath.controller.model.AddUserRequest;
import com.sampath.controller.model.AddUserResponse;
import com.sampath.entity.WellnessUser;
import com.sampath.exception.GlobalException;
import com.sampath.service.AddressService;
import com.sampath.service.UserService;
import com.sampath.service.model.AddAddressServiceRequest;
import com.sampath.service.model.AddAddressServiceResponse;
import com.sampath.service.model.AddUserServiceRequest;
import com.sampath.service.model.AddUserServiceResponse;

@RestController
@RequestMapping("/v1/address")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@Autowired
	private UserService userService;

	@Autowired
	private RestTemplate restTemplate;

//	@GetMapping("/{addressId}")
//	 public ResponseEntity<AddUserResponse> getUById(@PathVariable Long userId ) throws GlobalException{
//		
//		AddUserResponse response = new AddUserResponse();
//				AddUserServiceResponse serviceResponse=	userService.getUserById(userId);
//		response.setAge(serviceResponse.getAge());
//		response.setId(serviceResponse.getUserId());
//		response.setUsername(serviceResponse.getUsername());
//		return new ResponseEntity<AddUserResponse>(response,HttpStatus.OK);
//		
//	}

	@PostMapping
	public ResponseEntity<AddAddressResponse> saveAddress(@RequestBody AddAddressRequest request)
			throws GlobalException {

		// populate service request object
		AddAddressServiceRequest serviceReq = new AddAddressServiceRequest();
		serviceReq.setLine1(request.getLine1());
		serviceReq.setLine2(request.getLine2());
		serviceReq.setPinCode(request.getPinCode());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AddUserResponse> addUserResponse = restTemplate.exchange("http://USER-SERVICE/v1/users/1", HttpMethod.GET,
				requestEntity, AddUserResponse.class);  
		//if load balanced by spring cloud, don't use the localhost:port
		//It will throw an error
		WellnessUser user = new WellnessUser(addUserResponse.getBody().getId(), addUserResponse.getBody().getUsername(),
				addUserResponse.getBody().getAge());

		serviceReq.setUser(user);

		// service method call
		AddAddressServiceResponse serviceResponse = addressService.addAddress(serviceReq);
		AddAddressResponse addAddressResponse = new AddAddressResponse();
		addAddressResponse.setAddressId(serviceResponse.getAddressId());
		addAddressResponse.setLine1(serviceResponse.getLine1());
		addAddressResponse.setLine2(serviceResponse.getLine2());
		addAddressResponse.setPinCode(serviceResponse.getPinCode());
		addAddressResponse.setUser(serviceResponse.getUser());

		// service response object

		// populate controller repsonse object from service response object

		return new ResponseEntity<AddAddressResponse>(addAddressResponse, HttpStatus.CREATED);
	}

}
