package com.sampath.controller;

import java.util.HashMap;
//import java.net.http.HttpHeaders;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sampath.controller.model.AddUserRequest;
import com.sampath.controller.model.AddUserResponse;
import com.sampath.controller.model.UserList;
import com.sampath.exception.GlobalException;
import com.sampath.exception.NoContentException;
import com.sampath.exception.UserNotFoundException;
import com.sampath.repository.UserAddress;
import com.sampath.service.IUserService;
import com.sampath.service.model.AddUserServiceRequest;
import com.sampath.service.model.AddUserServiceResponse;
import com.sampath.service.model.UpdateUserServiceResponse;

@RestController
@RequestMapping("/v1/users")
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);


//	@Autowired
	private IUserService userService; // recommended
//	@Autowired

	public UserController(IUserService userService) {
		this.userService = userService;
	}

//	@Autowired
//	PasswordEncoder passwordEncoder;

//	@Autowired
//	UserRepository userRepository;

	@GetMapping
	public ResponseEntity<List<UserList>> getUserList() throws GlobalException {

		List<UserList> userList = userService.getUserList();
		LOG.info("After userService list");
		if (userList.isEmpty()) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			throw new NoContentException("Users list is Empty!");
		}

		return new ResponseEntity<>(userList, HttpStatus.OK);

	}

//	@GetMapping(value= "/test/{userId}")
	@GetMapping(value = "/test", produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<AddUserResponse> getUserById1(
			@RequestParam @NotBlank(message = "userId shouldn't blank") Long userId) throws GlobalException {

		AddUserResponse response = new AddUserResponse();
		AddUserServiceResponse serviceResponse = userService.getUserById(userId);
		response.setAge(serviceResponse.getAge());
		response.setId(serviceResponse.getUserId());
		response.setUsername(serviceResponse.getUsername());
//		LOG.info("encrypted value is: "+passwordEncoder.encode(serviceResponse.getUsername()));
		return new ResponseEntity<AddUserResponse>(response, HttpStatus.OK);

	}

	@GetMapping(value = "/{userId}")
//	@GetMapping(value= "/{userId}", produces=MediaType.APPLICATION_XML_VALUE)	
	public ResponseEntity<AddUserResponse> getUserById(@PathVariable Long userId) throws GlobalException {

		AddUserResponse response = new AddUserResponse();
		AddUserServiceResponse serviceResponse = userService.getUserById(userId);
		response.setAge(serviceResponse.getAge());
		response.setId(serviceResponse.getUserId());
		response.setUsername(serviceResponse.getUsername());
//		LOG.info("encrypted value is: "+passwordEncoder.encode(serviceResponse.getUsername()));
		return new ResponseEntity<AddUserResponse>(response, HttpStatus.OK);

	}

	@GetMapping(value = "/userAddress/{userId}")
//	@GetMapping(value= "/{userId}", produces=MediaType.APPLICATION_XML_VALUE)	
	public ResponseEntity<List<UserAddress>> getUserAddress(@PathVariable Long userId) throws GlobalException {

//		AddUserResponse response = new AddUserResponse();
//				AddUserServiceResponse serviceResponse=	userService.getUserById(userId);
//		response.setAge(serviceResponse.getAge());
//		response.setId(serviceResponse.getUserId());
//		response.setUsername(serviceResponse.getUsername());
//		LOG.info("encrypted value is: "+passwordEncoder.encode(serviceResponse.getUsername()));
//		userRepository.findUserWithAddress(userId);

//		return new ResponseEntity<List<UserAddress>>(userRepository.findUserWithAddress(userId),HttpStatus.OK);
		return null;
	}

	@PostMapping(value = "/test", consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<AddUserResponse> saveUser1(@RequestBody AddUserRequest request) throws GlobalException {

		// populate service request object
		AddUserServiceRequest serviceReq = new AddUserServiceRequest();
		serviceReq.setUsername(request.getUsername());
		serviceReq.setAge(request.getAge());

		// service method call
		AddUserServiceResponse serviceResponse = userService.addUser(serviceReq);
		AddUserResponse addUserResponse = new AddUserResponse();
		addUserResponse.setAge(serviceResponse.getAge());
		addUserResponse.setUsername(serviceResponse.getUsername());
		addUserResponse.setId(serviceResponse.getUserId());
		// service response object

		// populate controller repsonse object from service response object

		return new ResponseEntity<AddUserResponse>(addUserResponse, HttpStatus.CREATED);
	}

	@PostMapping
	public ResponseEntity<AddUserResponse> saveUser(@RequestBody AddUserRequest request) throws GlobalException {

		// populate service request object
		AddUserServiceRequest serviceReq = new AddUserServiceRequest();
		serviceReq.setUsername(request.getUsername());
		serviceReq.setAge(request.getAge());

		// service method call
		AddUserServiceResponse serviceResponse = userService.addUser(serviceReq);
		AddUserResponse addUserResponse = new AddUserResponse();
		addUserResponse.setAge(serviceResponse.getAge());
		addUserResponse.setUsername(serviceResponse.getUsername());
		addUserResponse.setId(serviceResponse.getUserId());
		// service response object

		// populate controller repsonse object from service response object

		return new ResponseEntity<AddUserResponse>(addUserResponse, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<AddUserResponse> udpateUser(@RequestBody AddUserRequest request)
			throws UserNotFoundException, GlobalException {

//		AddUserServiceResponse serviceResponse = new AddUserServiceResponse();
		AddUserResponse addUserResponse = new AddUserResponse();
		AddUserServiceResponse addServiceResponse = userService.getUserById(request.getUserId());
		AddUserServiceRequest serviceReq = new AddUserServiceRequest();

		if (addServiceResponse != null) {
			serviceReq.setUsername(request.getUsername());
			serviceReq.setAge(request.getAge());
			serviceReq.setId(request.getUserId());
			UpdateUserServiceResponse serviceResponse = userService.updateUser(serviceReq);
			addUserResponse.setAge(serviceResponse.getAge());
			addUserResponse.setUsername(serviceResponse.getUsername());
			addUserResponse.setId(serviceResponse.getUserId());
		} else
			throw new UserNotFoundException(request.getUserId() + " id is not found");

		return new ResponseEntity<AddUserResponse>(addUserResponse, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/test")
	public ResponseEntity<AddUserResponse> udpateUser1(@RequestBody AddUserRequest request)
			throws UserNotFoundException, GlobalException {
		
		AddUserResponse addUserResponse = userService.updateUser1(request);


//		AddUserResponse addUserResponse = new AddUserResponse();
//		AddUserServiceResponse addServiceResponse = userService.getUserById(request.getUserId());
//		AddUserServiceRequest serviceReq = new AddUserServiceRequest();
//
//		if (addServiceResponse != null) {
//			serviceReq.setUsername(request.getUsername());
//			serviceReq.setAge(request.getAge());
//			serviceReq.setId(request.getUserId());
//			UpdateUserServiceResponse serviceResponse = userService.updateUser(serviceReq);
//			addUserResponse.setAge(serviceResponse.getAge());
//			addUserResponse.setUsername(serviceResponse.getUsername());
//			addUserResponse.setId(serviceResponse.getUserId());
//		} else
//			throw new UserNotFoundException(request.getUserId() + " id is not found");

		return new ResponseEntity<AddUserResponse>(addUserResponse, HttpStatus.OK);
	}

	@PatchMapping
	public ResponseEntity<AddUserResponse> patchUser(@RequestBody AddUserRequest request)
			throws UserNotFoundException, GlobalException {

//		AddUserServiceResponse serviceResponse = new AddUserServiceResponse();
		AddUserResponse addUserResponse = new AddUserResponse();
		AddUserServiceResponse addServiceResponse = userService.getUserById(request.getUserId());
		AddUserServiceRequest serviceReq = new AddUserServiceRequest();

		if (addServiceResponse != null) {
			serviceReq.setUsername(addServiceResponse.getUsername());
			serviceReq.setAge(request.getAge());
			serviceReq.setId(addServiceResponse.getUserId());
			UpdateUserServiceResponse serviceResponse = userService.updateUser(serviceReq);
			addUserResponse.setAge(serviceResponse.getAge());
			addUserResponse.setUsername(serviceResponse.getUsername());
			addUserResponse.setId(serviceResponse.getUserId());
		} else
			throw new UserNotFoundException(request.getUserId() + " id is not found");

		return new ResponseEntity<AddUserResponse>(addUserResponse, HttpStatus.CREATED);
	}

	@DeleteMapping("/{userId}")
	public Map<String, Boolean> deletUserById(@PathVariable Long userId) throws GlobalException {
		Map<String, Boolean> responseMap = new HashMap<>();
		AddUserServiceResponse addServiceResponse = userService.getUserById(userId);
		if (addServiceResponse != null) {
			responseMap = userService.deleteUser(addServiceResponse.getUserId());
		}
		return responseMap;
	}

}
