package com.sampath.exception;

public class InternalServerErrorException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public InternalServerErrorException(String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}

}
