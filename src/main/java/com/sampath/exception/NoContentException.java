package com.sampath.exception;

public class NoContentException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public NoContentException(String message) {
		super(message);

	}

}
