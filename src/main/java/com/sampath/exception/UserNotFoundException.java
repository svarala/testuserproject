package com.sampath.exception;

public class UserNotFoundException extends GlobalException {

	public UserNotFoundException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
