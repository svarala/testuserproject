package com.sampath.exception;

public class FailedToGetUserException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public FailedToGetUserException(String message) {
		super(message);
	}

}
