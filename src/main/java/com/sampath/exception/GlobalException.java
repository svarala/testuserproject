package com.sampath.exception;

public class GlobalException extends Exception {

	public GlobalException(String message) {
		super(message);
	}
	private static final long serialVersionUID = 1L;

}
