package com.sampath.exception;

public class ResourceDeleteException extends InternalServerErrorException {

	private static final long serialVersionUID = 1L;

	public ResourceDeleteException(String message) {
		super(message);

	}

}
